﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace SwitchMateDriver
{
    public class SwitchMate
    {
        public enum RelayStates { Open, Close };

        //------------------
        // Properties
        //----------------

        //--------------------------------------------
        //  Switch-Mate data
        //--------------------------------------------
        private string comPort = string.Empty;
        private SerialPort serialPort;                          // place to save the Switch-Mate serial port handle
        private const int baud = 19200;                         // default baud rate
        private const string sm_prompt = "-> ";                 // prompt
        private const string sm_newline = "\r";                 // new line character
        private const string switchMateID = "SWITCH-MATE";      // leading part of the board ID. Read with SM_ID?
        private const string sm_dutPowerRelay = "01";              // relay number for DUT power
        private const string sm_pirSource = "02";                  // relay number for the PIR source

        private const string sm_select_relay = "SM_SR";         // select specific relay, rrs - rr= relay 01-08, s=0/1
        private const string sm_get_rly_status = "SM_RS?";      // get relay status
        private const string sm_master_reset = "SM_MR";         // master reset
        private const string sm_get_device_id = "SM_ID?";       // get device ID

        public SwitchMate(string port)
        {
            comPort = port;
        }


        /// <summary>
        /// Connect to the switch mate
        /// </summary>
        /// <returns>true = connected</returns>
        public void Connect(out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string readData = string.Empty;

            errorCode = 0;
            errorOccured = false;
            errorMsg = string.Empty;

            // setup the serial port
            serialPort = new SerialPort();
            serialPort.PortName = comPort;
            serialPort.BaudRate = baud;
            serialPort.ReadTimeout = 100;
            serialPort.NewLine = sm_newline;
            try
            {
                serialPort.Open();
                serialPort.WriteLine(sm_get_device_id);
                readData = serialPort.ReadTo(sm_prompt);
                Thread.Sleep(500);
                serialPort.WriteLine(sm_get_device_id);  // Do a second time just incase garbage
                readData = serialPort.ReadTo(sm_prompt);
                Thread.Sleep(500);
                if (!readData.Contains(switchMateID))
                {
                    errorMsg = "Switch-Mate ID is wrong. " + readData;
                    errorCode = -1111;
                    errorOccured = true;
                    return;
                }
            }
            catch (Exception ex)        // something went wrong getting the port open or reading the ID
            {
                errorMsg = "Switch-Mate open error : " + ex.Message;
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            return;
        }

        public void Disconnect()
        {
            if (serialPort.IsOpen)
                serialPort.Close();
            if (serialPort != null)
                serialPort.Dispose();
        }

        /// <summary>
        /// Will read the status of the relay channel. 0=open, 1=closed
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public bool IsChannelClosed(int channelNumber, out bool errorOccured, out int errorCode, out string errorMsg)
        {

            errorMsg = string.Empty;
            errorCode = 0;
            errorOccured = false;

            string responseString = string.Empty;

            if (serialPort == null)
            {
                errorMsg = "Switch Mate is not port is not connected.";
                errorCode = -1111;
                errorOccured = true;
                return false;
            }
            if (!serialPort.IsOpen)
            {
                errorMsg = "Switch Mate is not port is not connected.";
                errorCode = -1111;
                errorOccured = true;
                return false;
            }



            if ((channelNumber < 1) | (channelNumber > 8))
            {
                errorMsg = "channelName must be a interger value 1 to 8.  Found " + channelNumber;
                errorCode = -1111;
                errorOccured = true;
                return false;
            }

            try
            {
                serialPort.Write(sm_get_rly_status + "\r");
                responseString = serialPort.ReadTo(sm_prompt);
                if (responseString.Substring(responseString.IndexOf('<') + channelNumber, 1) == "1") // if showing closed
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                errorMsg = "Error getting relay status. " + ex.Message;
                errorCode = -1111;
                errorOccured = true;
                return false;
            }
        }

        /// <summary>
        /// Set a named channel.
        /// </summary>
        /// <param name="channelName">name of channel</param>
        /// <param name="value">value to set to</param>
        public void CloseChannel(int channelNumber, bool state, int afterActionDelayMS, out bool errorOccured, out int errorCode, out string errorMsg)
        {
            string responseString = string.Empty;

            errorMsg = string.Empty;
            errorCode = 0;
            errorOccured = false;

            if (serialPort == null)
            {
                errorMsg = "Switch Mate is not port is not connected.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }
            if (!serialPort.IsOpen)
            {
                errorMsg = "Switch Mate is not port is not connected.";
                errorCode = -1111;
                errorOccured = true;
                return;
            }


            try
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                if (state)
                {
                    serialPort.Write(sm_select_relay + channelNumber.ToString("D2") + "1\r");
                }
                else
                {
                    serialPort.Write(sm_select_relay + channelNumber.ToString("D2") + "0\r");
                }
                serialPort.ReadTo(sm_prompt);            // wait for the command response prompt
                // verify that it was set
                serialPort.Write(sm_get_rly_status + "\r");
                responseString = serialPort.ReadTo(sm_prompt);
                if (responseString.Substring(responseString.IndexOf('<') + channelNumber, 1) == "1") // if showing closed
                {
                    if (!state)      // if showing closed when it should be open
                    {
                        errorCode = -1111;
                        errorMsg = "Relay not in correct state";
                        errorOccured = true;
                        return;
                    }
                }
                Thread.Sleep(afterActionDelayMS);                              // so next does not come to quick
            }
            catch (Exception ex)
            {
                errorCode = -1111;
                errorMsg = "Error issuing relay set command. " + ex.Message;
                errorOccured = true;
                return;

            }
            return;
        }

    }
}
